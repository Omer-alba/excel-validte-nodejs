require('regenerator-runtime/runtime');
const Excel = require('exceljs/dist/es5');
const fs = require('fs');
const path = require('path');

let allElements = [];
const workbook = new Excel.Workbook();

module.exports = async function createExcelJson(file, contentPartnerName, filename ,local) {
    let workBookPromise = undefined;
    if (!local){
       workBookPromise  = workbook.xlsx.load(file.data);
    } else {
        workBookPromise = workbook.xlsx.readFile(file)
    }

    return  workBookPromise.then(function () {
        const allAvaibleSheetsname = [];
        workbook.eachSheet(worksheet => {
            allAvaibleSheetsname.push(worksheet.name);
        });

        //workbook.xlsx.writeFile(`C:\\Users\\OmerAlBadry\\Downloads\\test.xlsx`);
        fs.mkdir(path.join(__dirname, contentPartnerName),
            {recursive: true}, (err) => {
                if (err) {
                    return console.error(err);
                }
                console.log('Directory created successfully!');
            });
        workbook.xlsx.writeFile(`./${contentPartnerName}/${filename}`);

        console.time("beginn");
        loobForEachSheet(allAvaibleSheetsname);
        console.timeEnd("beginn");
        return showAllElements();
    });
}

const loobForEachSheet = function (sheetsName) {
    for (let ind = 0; ind < sheetsName.length; ind++) {
        if (sheetsName[ind].includes('structions'))
            continue;

        let worksheet = workbook.getWorksheet(sheetsName[ind]);

        for (var i = 1; i <= worksheet.actualColumnCount; i++) {
            let name, type, name_long, description, valueList = [], value;

            for (var j = 1; j <= worksheet.actualRowCount; j++) {
                switch (true) {
                    case j === 1:
                        name = worksheet.getRow(j).getCell(i).value
                        break;
                    case j === 2:
                        type = worksheet.getRow(j).getCell(i).value
                        break;
                    case j === 3:
                        name_long = worksheet.getRow(j).getCell(i).value
                        break;
                    case j === 4:
                        description = worksheet.getRow(j).getCell(i).toString()
                        break;
                    case (j >= 5):
                        valueList.push({
                            "value": isValueValid(worksheet.getRow(j).getCell(i).value),
                            "row_number": j
                        })
                        break;
                }
            }// end second for loob

            allElements.push({
                "sheet_name": worksheet.name,
                "column": numToSSColumn(i),
                "row_number": 0,
                "name": name || null,
                "type": type || null,
                name_long,
                description,
                valueList,
                value
            })

        }// end first for loob
    }
}

function numToSSColumn(num) {
    var s = '', t;

    while (num > 0) {
        t = (num - 1) % 26;
        s = String.fromCharCode(65 + t) + s;
        num = (num - t) / 26 | 0;
    }
    return s || undefined;
}

function isValueValid(value) {
    if (typeof value === "undefined") return null;
    if (typeof value == "string" && !value.trim()) return null;

    return value
}

function showAllElements() {
    const result = allElements;
    allElements = [];
    return result;
}
