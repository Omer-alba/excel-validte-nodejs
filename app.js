const express = require('express');
const app = express();

const http = require('http').Server(app).listen(3001);
const upload = require('express-fileupload');
const {body, validationResult} = require('express-validator');
const fs = require('fs');
const cors = require('cors');

// Files
const createExcelJson = require('./excelReader');
const EditXlsxFile = require('./EditXlsxFile');
const {request, response} = require("express");
const Database = require('./utils/Database');
app.use(upload());

let myResult, filename, pathname, contentPartnerName, contentPartnerPassword;

async function treiber(file, filename, isLocal) {
    const result = await createExcelJson(file, contentPartnerName, filename, isLocal);
    return result;
}

app.post('/upload',
    body('contentPartnerName', 'Content Partner Name is missing').trim().isLength({min: 1}).escape(),
    body('contentPartnerPassword', 'Password is missing').trim().isLength({min: 1}).escape(),
    (request, response, next) => {

        //file is avaible?
        if (!request.files) {
            return response.redirect(request.body.pathname + `/importExportView?message=please select an excel file`);
        }

        //validation:
        const errors = validationResult(request);

        if (!errors.isEmpty()) {
            return response.redirect(request.body.pathname + `/importExportView?message=${errors.errors[0].msg}`);
        } else if (!request.files.file.name.includes('.xlsx')) {
            return response.redirect(request.body.pathname + `/importExportView?message=invalid file, please upload only excel file (.xlsx)`);
        }

        contentPartnerName = request.body.contentPartnerName;

        try {

            const promise = treiber(request.files.file, request.files.file.name, false);
            promise.then(result => {
                myResult = JSON.stringify(result);

                const regex = / /g;
                filename = request.files.file.name.replace(regex,'');

                contentPartnerPassword = request.body.contentPartnerPassword;
                pathname = request.body.pathname;

                response.redirect(request.body.pathname + '/validation');
            });

        } catch (error) {
            console.log('error: ', error)
        }
    });

app.get('/result', (request, response) => {
    response.writeHead(200, {'Content-Type': 'text/html'});
    response.end(myResult);
});

app.get('/filename', (request, response) => {
    response.writeHead(200, {'Content-Type': 'text/html'});
    response.end(JSON.stringify({
        'filename': filename,
        'contentPartnerName': contentPartnerName,
        'contentPartnerPassword': contentPartnerPassword
    }));
});

app.get('/clear', (request, response) => {
    response.writeHead(200, {'Content-Type': 'text/html'});
    filename = null;
    myResult = null;
    response.end();
});

app.get('/test', (request, response) => {
    response.writeHead(200, {'Content-Type': 'text/html'})
    response.end('läuft!');
});

app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'}));

app.post(['/writeExcel/invalid', '/writeExcel/language', '/writeExcel/spelling', '/writeExcel/country/'], (request, response) => {
    const contents = request.body;

    const editXlxsFile = new EditXlsxFile(contents, contentPartnerName);
    const result = editXlxsFile.run(contents);

    result.then(() => response.end());
});

app.post('/writeExcel/diverse', (request, response) => {
    const contents = request.body;
    const editXlxsFile = new EditXlsxFile(contents, contentPartnerName);
    const result = editXlxsFile.handleDiverse();

    result.then(() => response.end());
});

app.get('/xlxs-download', cors({
    exposedHeaders: ['Content-Disposition'],
}), async (request, response) => {
    const path = `./${contentPartnerName}/${filename}`;
    const stream = fs.createReadStream(path);

    response.set({
        'Content-Disposition': `attachment; filename=${filename}`,
        'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });
    stream.pipe(response);
});

app.post('/continue', (request, response) => {
    const promise = treiber(`./${contentPartnerName}/${filename}`, filename, true);

    promise.then(result => {
        myResult = JSON.stringify(result);
        //delete the sql shit!
        const sql = new Database();
        sql.connect({host: '127.0.0.1', user: 'root', password: '', databaseName: 'myhobbies-learn'});
        sql.delete('error_sampler',`${contentPartnerName}-${request.body.errorType}`);

        response.redirect('http://127.0.0.1:8000/validation');
    });
});





