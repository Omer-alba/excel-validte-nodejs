const Database = require('./utils/Database');
const Excel = require("exceljs");
const {response} = require("express");

function EditXlsxFile(body, contentPartnerName) {
    this.body = body;
    this.contentPartnerName = contentPartnerName;
    this.filename = '';
}

EditXlsxFile.prototype.run = function () {
    const Excel = require('exceljs');
    const workbook = new Excel.Workbook();

    let {
        data,
        toReplace,
        errorType
    } = this.body

    let jsonData = JSON.parse(data);
    const jsonToReplace = JSON.parse((toReplace || '{}'));
    this.filename = jsonData[0].filename;

    const {
        filename,
        contentPartnerName
    } = this;

    // path has to be replace from \\ to / linux
    const editXlsxValuePromise = workbook.xlsx.readFile(`./${contentPartnerName}/${filename}`)

    const storeInDatabasePromise = editXlsxValuePromise.then(() => {
        for (const [index, container] of jsonData.entries()) {
            if (Object.keys(jsonToReplace).length > 0 && !jsonToReplace[`selected-value-${index}`]) continue;

            let {
                row,
                cell,
                worksheetName,
                content,
                status
            } = container.content;

            // replace inside xml file the current value
            try {
                const worksheet = workbook.getWorksheet(worksheetName);
                const row_ = worksheet.getRow(row);
                row_.getCell(cell).value = jsonToReplace[`selected-value-${index}`] || content.toLowerCase();
                row_.commit();

                // what has been changed array
                container['editContent'] = jsonToReplace[`selected-value-${index}`] || content.toLowerCase();
                status.successful = true;
            } catch (error) {
                status.successful = false;
                status.message = error
            }

        }

        return {
            'content': jsonData,
            'errorKind': contentPartnerName + '-' + errorType
        }
    });

    const wirteIntoXlsxPromise = storeInDatabasePromise.then((obj) => {
        const sql = new Database();
        sql.connect({host: '127.0.0.1', user: 'root', password: '', databaseName: 'myhobbies-learn'});
        sql.updater(obj.content, obj.errorKind);
    });

    return wirteIntoXlsxPromise.then((response) => {
        workbook.xlsx.writeFile(`./${contentPartnerName}/${filename}`);
        return true;
    }).catch((response, e) => {
        console.log(e);
        console.log(response);
        return false;
    });
}

EditXlsxFile.prototype.handleDiverse = function () {
    const Excel = require('exceljs');
    const workbook = new Excel.Workbook();

    let {
        toReplace,
        data,
        errorType
    } = this.body;

    let {contentPartnerName} = this;
    jsonToReplace = JSON.parse(toReplace)
    jsonData = JSON.parse(data);
    const filename = jsonData[Object.keys(jsonData)[0]].filename;

    const editXlsxValuePromise = workbook.xlsx.readFile(`./${contentPartnerName}/${filename}`);

    const storeInDatabasePromise = editXlsxValuePromise.then(() => {
        for (const key of Object.keys(jsonData)) {
            for (let index = 0; index < jsonData[key].elements.length; index++) {
                const container = jsonData[key].elements[index];

                const form_name = jsonData[key].elements[index]['form_name'] + '-' + index

                if (!jsonToReplace[form_name]) continue;

                let {
                    row,
                    cell,
                    worksheetName,
                    content,
                    status
                } = container.content;

                // replace inside xml file the current value
                try {
                    const worksheet = workbook.getWorksheet(worksheetName);
                    const row_ = worksheet.getRow(row);
                    row_.getCell(cell).value = jsonToReplace[form_name];
                    row_.commit();

                    // what has been changed array
                    container['editContent'] = jsonToReplace[form_name];
                    status.successful = true;
                } catch (error) {
                    status.successful = false;
                    status.message = error
                }
            }
        }

        return {
            'content': jsonData,
            'errorType': contentPartnerName + '-' + errorType
        }
    });

    const wirteIntoXlsxPromise = storeInDatabasePromise.then((obj) => {
        const sql = new Database();
        sql.connect({host: '127.0.0.1', user: 'root', password: '', databaseName: 'myhobbies-learn'});
        sql.updater(obj.content, obj.errorType);
    });

    return wirteIntoXlsxPromise.then(() => {
        workbook.xlsx.writeFile(`./${contentPartnerName}/${filename}`);
        return true;
    }).catch((response, e) => {
        console.log(e);
        console.log(response);
        return false;
    });
}

EditXlsxFile.prototype.saveChanges = function (data, errorKind) {
    const database = new Database();
    database.connect({
        host: '127.0.0.1',
        user: 'root',
        password: '',
        database: 'myhobbies-learn'
    });

    database.updater(data, errorKind);
    return true;
}

module.exports = EditXlsxFile;
