const mysql = require("mysql");

function Databse() {
    this.database = null;
}

Databse.prototype.connect = function ({host, user, password, databaseName}) {
    this.database = mysql.createConnection({
        host: host,
        user: user,
        password: password,
        database: databaseName,
    });

    this.database.connect(function (err) {
        if (err) throw err;
        console.log('db connected');
    });
}

Databse.prototype.updater = function (content, errorType) {
    const sql = `UPDATE error_sampler
                 SET content = ?
                 WHERE name = ?`;

    const data = [
        JSON.stringify(content),
        errorType,
    ];

    this.database.query(sql, data, function (error, result) {
        if (error) {
            console.log('error: ',error)
        };
        console.log('u was succesfully');
        return result || true
    });
}

Databse.prototype.delete = function (tableName, errorType) {
    const sql = `DELETE
                 FROM ${tableName}
                 WHERE name = "${errorType}"`

    this.database.query(sql, function (error, result) {
        if (error) {
            console.log(error);
            return false
        };
        console.log('d was succesfully');
        return result || true;
    });
}

module.exports = Databse;